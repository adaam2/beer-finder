# Dependencies

You will need to install Ruby (preferably version 2.3.1) using rbenv, or something similiar.

Yarn / NPM for the client project

Finally you will need to install the `Foreman` gem (`gem install foreman`).

# Setup

- `cd` to app directory, then run `bundle install` to install all of the relevant ruby gems
- `cd` again to the sub directory `client`, and run `yarn install`
- `cd` back up to the parent directory, and run `foreman start -f Procfile.dev`. If nothing has failed (which I hope it won't!), then you should be able to navigate to the application in your browser at `http://localhost:3000`


# Technical description

## Server-side

I use Sinatra which is a micro web framework for Ruby. There wasn't really a need to use Rails for this application.

I make use of `Rack Proxy` to namespace the server-side code to `/api`, and to proxy the webpack dev server instance from port `5000` through to the normal `3000` port.

You can run the specs for the server side code by just running `rspec` in the root directory after having run `bundle install`. You may need to run rspec using `bundle exec rspec` if you run into environment errors.

## Client-side

I have a lot of experience with modern client-side frameworks like React and Redux, so therefore have a few boilerplate projects that I have designed before which make use of some interesting client-side libraries, such as `Reselect`, and `Redux`. They are on show in this demo project.
