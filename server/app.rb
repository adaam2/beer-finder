require 'sinatra/base'
require_relative './routes'

class App < Sinatra::Base
  include Routing
end
