require 'sinatra/json'
require 'require_all'

require_all 'server/services'

module Routing
  def self.included(app)
    app.get '/' do
      erb :home
    end

    app.post '/beers' do
      json beer_service.search(query)
    end

    app.get '/beers' do
      json beer_service.find(id)
    end
  end

  private

  def id
    params[:id]
  end

  def query
    params[:query]
  end

  def beer_service
    @beer_service ||= BeerService.new
  end
end
