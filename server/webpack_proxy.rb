require 'rack-proxy'

class WebpackProxy < Rack::Proxy
  def rewrite_env(env)
    env['HTTP_HOST'] = 'localhost:5000'
    env
  end
end