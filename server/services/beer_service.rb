require 'faraday'
require 'active_support/all'

class BeerService
  attr_accessor :connection

  def initialize(conn = nil)
    @connection = conn || connection_setup
  end

  def search(query)
    raise(ArgumentError, 'Please provide a query') unless query
    get('beers', name: query, hasLabels: 'Y')
  end

  def find(id)
    raise(ArgumentError, 'Please provide an id') unless id
    get("beer/#{id}")
  end

  private

  def get(path, params = {})
    JSON.parse(
      @connection.get(
        "#{path}?#{merged_params(params).to_query}"
      ).body
    )
  end

  def merged_params(params)
    params.merge(key: ENV.fetch('BEER_KEY'))
  end

  def connection_setup
    Faraday.new(ENV.fetch('BEER_BASE_URL')) do |faraday|
      faraday.response :logger, ::Logger.new(STDOUT), bodies: true
      faraday.adapter Faraday.default_adapter
    end
  end
end
