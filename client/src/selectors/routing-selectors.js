import { createSelector } from 'reselect';

export const getRoutingStore = store => store.router;

export const getDetailId = createSelector(
  [getRoutingStore],
  (routingStore) => {
    const path = routingStore.location.pathname;
    return path.replace("/detail/", "");
  }
);