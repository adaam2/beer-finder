import React from 'react';
import { Switch, Route } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import { history } from '../store';
import Layout from '../containers/Layout';
import App from '../containers/App';
import Detail from '../components/DetailPage';

const routes = (
  <ConnectedRouter history={history}>
    <Layout>
      <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/detail/:id" component={Detail} />
      </Switch>
    </Layout>
  </ConnectedRouter>
);

export default routes;