import { ACTION_TYPES } from '../constants/action-types';

const initialState = {
  loaded: false,
  results: []
};

export default function app(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.APP_LOAD:
      return { ...state, loaded: true };
    case ACTION_TYPES.BEER.SEARCH_SUCCESS:
      return { ...state, results: action.results };
    case ACTION_TYPES.BEER.SEARCH_FAILURE:
      return { ...state, error: action.error };
    case ACTION_TYPES.BEER.FIND_INDIVIDUAL_BEER_SUCCESS:
      return { ...state, result: action.beer };
    case ACTION_TYPES.BEER.FIND_INDIVIDUAL_BEER_FAILURE:
      return { ...state, error: action.error };
    default:
      return state;
  }
}