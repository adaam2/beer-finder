import React from 'react';
import { connect }  from 'react-redux';

import SearchBox from '../components/SearchBox';
import SearchResults from '../components/SearchResults';

import { loadApp } from '../actions/app';
// eslint-disable-next-line
import styles from './App.css';

type Props = {
  dispatch: () => void,
  loaded: boolean
}

class App extends React.Component {
  props: Props;

  componentDidMount() {
    this.props.dispatch(loadApp());
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>
            Find your beer!
          </h1>
          <SearchBox />
        </div>
        <div className="App-Results">
          <SearchResults />
        </div>
      </div>
    );
  }
}

export default connect((store, props) => {
  return {
    loaded: store.app.loaded,
  };
})(App);