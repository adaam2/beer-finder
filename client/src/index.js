import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

// eslint-disable-next-line
import styles from './index.css';

import store from './store';
import routes from './routes';

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        {routes}
      </Provider>
    </AppContainer>,
    document.getElementById('root'),
  )
}

render();

if (module && module.hot) {
  module.hot.accept('./containers/App', () => {
    render();
  });
}