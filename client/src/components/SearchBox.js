import React, { Component } from 'react';
import { connect }  from 'react-redux';
import _ from 'lodash';

import * as actions from '../actions/app';

// eslint-disable-next-line
import styles from './SearchBox.css';

type Props = {
  dispatch: () => void,
  loaded: boolean
}

class SearchBox extends Component {
  props: Props;

  constructor(props) {
    super(props);

    this.search = this.search.bind(this);
  }

  search(event) {
    this.props.dispatch(_.debounce(actions.findBeers(event.target.value)));
  }

  render() {
    return (
      <div className="SearchBox">
        <input type="search" className="SearchBox-Input" onChange={this.search} placeholder="Try 'Lager'" />
      </div>
    );
  }
}

export default connect((store, props) => {
  return {
    loaded: store.app.loaded,
  };
})(SearchBox);