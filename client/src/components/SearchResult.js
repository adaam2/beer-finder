import React, { Component } from 'react';
import { connect }  from 'react-redux';

import * as actions from '../actions/app';

type Props = {
  result: object,
  dispatch: () => void
}

class SearchResult extends Component {
  props: Props;

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.props.dispatch(actions.navigateToDetailPage(this.props.result.id));
  }

  render() {
    return (
      <div className="SearchResult" onClick={this.handleClick}>
        <img alt="" src={this.props.result.labels.icon} />
        <div className="SearchResult-Content">
          <h2>{this.props.result.name}</h2>
          <p>{this.props.result.description}</p>
        </div>
      </div>
    );
  }
}

export default connect((store, props) => {
  return {
    result: props.result,
  };
})(SearchResult);