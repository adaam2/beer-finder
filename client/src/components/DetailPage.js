import React, { Component } from 'react';
import { connect }  from 'react-redux';

import { getDetailId } from '../selectors/routing-selectors';
import * as actions from '../actions/app';

import styles from './DetailPage.css';

type Props = {
  id: integer,
  result: object,
  dispatch: () => void
}

class DetailPage extends Component {
  props: Props;

  componentDidMount() {
    this.props.dispatch(actions.findIndividualBeer(this.props.id));
  }

  strength() {
    if (!this.props.result.style) {
      return (null);
    }

    return (
      <div className="DetailPage-Strength">
        <p>
          <strong>ABV:</strong> {this.props.result.style.abvMin}% - {this.props.result.style.abvMax}%
        </p>
      </div>
    );
  }

  description() {
    if(!this.props.result.style) {
      return (null);
    }

    return (
      <p>
        {this.props.result.style.description}
      </p>
    );
  }

  render() {
    if (!this.props.result) {
      return (null);
    }

    return (
      <div className="DetailPage">
        <img alt="" src={this.props.result.labels.medium} />

        <div className="DetailPage-Content">
          <h1>
            {this.props.result.name}
          </h1>

          {this.strength()}

          {this.description()}
        </div>
      </div>
    );
  }
}

export default connect((store, props) => {
  return {
    id: getDetailId(store),
    result: store.app.result
  };
})(DetailPage);