import React, { Component } from 'react';
import { connect }  from 'react-redux';
import _ from 'lodash';

import SearchResult from './SearchResult';

import './SearchResults.css';

type Props = {
  dispatch: () => void,
  results: array
}

class SearchResults extends Component {
  props: Props;

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.results !== this.props.results) {
      return true;
    }

    return false;
  }

  results = () => {
    return _.map(this.props.results, (result) => {
      return (
        <SearchResult key={result.id} result={result} />
      );
    });
  }

  render() {
    if(!this.props.results) {
      return (
        <div className="SearchResults">
          <p>None</p>
        </div>
      );
    }

    return (
      <div className="SearchResults">
        {this.results()}
      </div>
    )
  }
}

export default connect((store, props) => {
  return {
    results: store.app.results,
  };
})(SearchResults);