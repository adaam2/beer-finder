import { ACTION_TYPES } from '../constants/action-types';
import ApiClient from '../shared/api-client';
import { beerUri } from '../constants/config';
import { push } from 'connected-react-router';

export const loadApp = () => {
  return {
    type: ACTION_TYPES.APP_LOAD,
  };
}

export const changedQuery = (query) => (dispatch) => {
  dispatch(push(`?query=${query}`));
}

export const findBeersSuccess = (results) => {
  return {
    type: ACTION_TYPES.BEER.SEARCH_SUCCESS,
    results
  };
}

export const findBeersFailure = (error) => {
  return {
    type: ACTION_TYPES.BEER.SEARCH_FAILURE,
    error
  };
}

export const findBeers = (query) => {
  return (dispatch) => {
    dispatch(changedQuery(query));

    return new Promise((resolve, reject) => {
      return ApiClient.post(`${beerUri}?query=${query}`)
        .then(
          response => {
            const results = response.body.data;

            dispatch(findBeersSuccess(results));
            resolve(results);
          },
          error => {
            dispatch(findBeersFailure(error));
            reject(error);
          }
        );
    });
  }
}

export const findIndividualBeerSuccess = (beer) => {
  return {
    type: ACTION_TYPES.BEER.FIND_INDIVIDUAL_BEER_SUCCESS,
    beer
  }
}

export const findIndividualBeerFailure = (error) => {
  return {
    type: ACTION_TYPES.BEER.FIND_INDIVIDUAL_BEER_FAILURE,
    error
  }
}

export const findIndividualBeer = (id) => {
  return (dispatch) => {
    return new Promise((resolve, reject) => {
      return ApiClient.get(`${beerUri}?id=${id}`)
        .then(
          response => {
            const result = response.body.data;
            dispatch(findIndividualBeerSuccess(result));
            resolve(result);
          },
          error => {
            dispatch(findIndividualBeerFailure(error));
            reject(error);
          }
        );
    });
  }
}

export const navigateToDetailPageSuccess = (id) => {
  return {
    type: ACTION_TYPES.BEER.NAVIGATE_TO_DETAIL_PAGE,
    id
  };
}

export const navigateToDetailPage = (id) => (dispatch) =>  {
  dispatch(navigateToDetailPageSuccess(id));

  dispatch(push(`/detail/${id}`));
}