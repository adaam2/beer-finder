require 'sinatra_helper'
require 'faraday'

describe BeerService, type: :service do
  let(:connection) { instance_double Faraday::Connection }
  let(:api_key) { 'fake_key' }
  let(:base_url) { 'http://api.brewerydb.com/v2/' }

  before do
    allow(connection)
      .to receive(:get)
      .with(any_args)
      .and_return(response)

    allow(ENV)
      .to receive(:fetch)
      .with('BEER_KEY')
      .and_return(api_key)

    allow(ENV)
      .to receive(:fetch)
      .with('BEER_BASE_URL')
      .and_return(base_url)
  end

  let(:body) { '{}' }
  let(:response) { instance_double Rack::Response, body: body }

  subject { described_class.new(connection) }

  describe 'injection of Faraday connection' do
    it 'should allow injecting a custom Faraday instance' do
      expect(subject.connection)
        .to eq connection
    end
  end

  describe '#search' do
    let(:query) { 'lager' }

    it 'should send the query param along with the api key in the query string' do
      expect(connection)
        .to receive(:get)
        .with("beers?hasLabels=Y&key=#{api_key}&name=#{query}")

      subject.search(query)
    end

    it 'should return the result' do
      expect(subject.search(query))
        .to eq JSON.parse(body)
    end

    context 'When the query is blank' do
      let(:query) { nil }

      it 'should raise an exception' do
        expect {
          subject.search(query)
        }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#find' do
    let(:id) { "828" }

    it 'should send a request to the correct endpoint with correctly formatted query string' do
      expect(connection)
        .to receive(:get)
        .with("beer/#{id}?key=#{api_key}")

      subject.find(id)
    end

    it 'should return the result of JSON.parse body' do
      expect(
        subject.find(id)
      ).to eq JSON.parse(body)
    end

    context 'When no ID is provided' do
      let(:id) { nil }

      it 'should raise an ArgumentError' do
        expect {
          subject.find(id)
        }.to raise_error(ArgumentError)
      end
    end
  end
end
