require 'dotenv'
require 'require_all'

require_all './server'

# Load environment variables
Dotenv.load('.env.development')

# Namespace the API to /api using Rack proxy
run Rack::URLMap.new(
  '/api' => App,
  '/' => WebpackProxy.new
)
